package org.gi3.bookapp;

import org.gi3.bookapp.entities.AppUser;
import org.gi3.bookapp.entities.Book;
import org.gi3.bookapp.repositories.AppUserRepository;
import org.gi3.bookapp.repositories.BookRepository;
import org.gi3.bookapp.security.entities.AppRole;
import org.gi3.bookapp.security.entities.AuthAppUser;
import org.gi3.bookapp.security.services.AccountServices;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;

@SpringBootApplication
public class BookappApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookappApplication.class, args);
    }

    @Bean
    BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/user/**").allowedOrigins("*");
            }
        };
    }

    @Bean
    CommandLineRunner start(AppUserRepository appUserRepository,
                            BookRepository bookRepository,
                            AccountServices accountServices) {
        return args -> {
            /*AppUser appUser1 = appUserRepository.save(new AppUser(null, "Ahmed", "064856452", "ahmed@ahmed", "Av 462 VVCC", null));
            AppUser appUser2 = appUserRepository.save(new AppUser(null, "Kamal", "0649586545", "kamal@kamal", "Av 462 GGTRD", null));

            bookRepository.save(new Book(null, "The Lord of the Rings", "J. R. R. Tolkien", "1954", "Set in Middle-earth, the world at some distant time in the past., the story began as a sequel to Tolkien's 1937 children's book The Hobbit, but eventually developed into a much larger work.", 150.0, null, "Fantasy", false, appUser1));
            bookRepository.save(new Book(null, "The Name of the Wind", "Patrick Rothfuss", "2007", "The Name of the Wind is a heroic fantasy novel written by American author Patrick Rothfuss. It is the first book in the ongoing fantasy trilogy The Kingkiller Chronicle, followed by The Wise Man's Fear.", 152.0, null, "Fantasy", false, appUser1));
            bookRepository.save(new Book(null, "A Wizard of Earthsea", "Ursula K. Le Guin", "1968", "A Wizard of Earthsea is a fantasy novel written by American author Ursula K. Le Guin and first published by the small press Parnassus in 1968. It is regarded as a classic of children's literature.", 152.0, null, "Fantasy", false, appUser2));
            bookRepository.save(new Book(null, "Dune", " Frank Herbert", "1965", "Dune is a 1965 science-fiction novel by American author Frank Herbert, It tied with Roger Zelazny's This Immortal for the Hugo Award in 1966, and it won the inaugural Nebula Award for Best Novel.", 152.0, null, "Science Fiction", false, appUser2));
            bookRepository.save(new Book(null, "Neuromancer", "William Gibson", "1984", "Neuromancer is one of the best-known works in the cyberpunk genre and the only novel to win the Nebula Award, the Philip K. Dick Award, and the Hugo Award.", 152.0, null, "Science Fiction", false, appUser1));
            bookRepository.save(new Book(null, "Brave New World", " Aldous Huxley", "1932", "Brave New World is a dystopian social science fiction novel by English author Aldous Huxley, written in 1931 and published in 1932.", 150.0, null, "Science Fiction", false, appUser1));
            bookRepository.save(new Book(null, "Treasure Island", "Robert Louis Stevenson", "1883", "Treasure Island is an adventure novel by Scottish author Robert Louis Stevenson, narrating a tale of \"buccaneers and buried gold.", 152.0, null, "Adventure", false, appUser2));
            bookRepository.save(new Book(null, "Into Thin Air", "Jon Krakauer", "1997", "Everest Disaster is a 1997 bestselling non-fiction book details Krakauer's experience in the 1996 Mount Everest disaster.", 152.0, null, "Adventure", false, appUser2));
            bookRepository.save(new Book(null, "The Call of the Wild", "Jack London", "1903", "The Call of the Wild is a short adventure set in Yukon, Canada, during the 1890s Klondike Gold Rush, when strong sled dogs were in high demand.", 152.0, null, "Adventure", false, appUser1));
            bookRepository.save(new Book(null, "The Duke and I", "Julia Quinn", "2000", "A #1 New York Times bestseller the first novel in the beloved Regency-set world of her charming, powerful Bridgerton family, now a series created by Shonda Rhimes for Netflix.", 152.0, null, "Romance", false, appUser2));
            bookRepository.save(new Book(null, "Pride and Prejudice", "Jane Austen", "1813", "The novel follows the character development of Elizabeth Bennet, the dynamic protagonist of the book who learns about the repercussions of hasty judgments.", 150.0, null, "Romance", false, appUser1));
            bookRepository.save(new Book(null, "Jane Eyre", "Charlotte Bronte", "1847", "Published under the pen name \"Currer Bell\", on 16 October 1847, by Smith, Elder & Co. of London. The first American edition was published the following year by Harper & Brothers of New York.", 152.0, null, "Romance", false, appUser2));
            bookRepository.save(new Book(null, "Dracula", "Bram Stoker", "1897", "Dracula is an 1897 Gothic horror novel It introduced the character of Count Dracula and established many conventions of subsequent vampire fantasy.", 152.0, null, "Horror", false, appUser1));
            bookRepository.save(new Book(null, "Frankenstein", "Mary Shelley", "1823", "Frankenstein; or, The Modern Prometheus is an 1818 novel written by English author Mary Shelley. Frankenstein tells the story of Victor Frankenstein, who creates a sapient creature in an unorthodox scientific experiment.", 152.0, null, "Horror", false, appUser2));
            bookRepository.save(new Book(null, "The Haunting of Hill House", "Shirley Jackson", "1959", "The Haunting of Hill House is a 1959 gothic horror novel. A finalist for the National Book Award and considered one of the best literary ghost stories published during the 20th century.", 152.0, null, "Horror", false, appUser1));


            AuthAppUser appUser = accountServices.saveUser(new AuthAppUser(null, "kamal@kamal", "1234", new ArrayList<>()));
            AppRole appRole = accountServices.saveRole(new AppRole(null, "USER"));
            accountServices.addRoletoUser("kamal@kamal", "USER");

            AuthAppUser appUser3 = accountServices.saveUser(new AuthAppUser(null, "ahmed@ahmed", "1234", new ArrayList<>()));
            accountServices.addRoletoUser("ahmed@ahmed", "USER");*/
        };
    }
}
