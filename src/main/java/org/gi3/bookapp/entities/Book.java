package org.gi3.bookapp.entities;


import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor @NoArgsConstructor @ToString
public class Book {
    @Id @GeneratedValue
    private Long id;
    @NotNull(message = "Title Cannot Be Null")
    @Size(min = 4)
    private String title;
    @NotNull(message = "Author Cannot Be Null")
    @Size(min = 4)
    private String writer;
    @NotNull(message = "Year Cannot Be Null")
    @Size(min = 4)
    private String year;
    @NotNull(message = "Description Cannot Be Null")
    @Size(min = 10)
    private String description;
    @NotNull(message = "Price Cannot Be Null")
    private double price;
    private String imagePath;
    @NotNull(message = "Category Cannot Be Null")
    @Size(min = 4)
    private String category;
    private boolean soldOut;
    @ManyToOne
    private AppUser seller;
}
