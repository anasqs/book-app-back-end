package org.gi3.bookapp.web;

import org.gi3.bookapp.entities.AppUser;
import org.gi3.bookapp.entities.Book;
import org.gi3.bookapp.services.AppUserServices;
import org.gi3.bookapp.services.BookServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/user")
public class AppUserRestController {

    @Autowired
    private BookServices bookServices;
    @Autowired
    private AppUserServices appUserServices;

    @GetMapping("/auth")
    public AppUser getAppUserById() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        return appUserServices.getAppUserByEmail(email);
    }

    @PostMapping("/books")
    public Book saveBook(@RequestBody @Valid Book book) {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        AppUser appUser = appUserServices.getAppUserByEmail(email);
        book.setSeller(appUser);
        book.setSoldOut(false);
        return bookServices.saveBook(book);
    }

    @GetMapping("/myBooks")
    public Page<Book> getBooksBySeller(@RequestParam(name = "page", defaultValue = "0") int page,
                                       @RequestParam(name = "size", defaultValue = "5") int size) {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        AppUser appUser = appUserServices.getAppUserByEmail(email);
        return bookServices.getBooksBySeller(appUser.getId(), page, size);
    }

    @GetMapping("/myBooksByCat")
    public Page<Book> getBooksBySellerAndCat(@RequestParam(name = "catName") String catName,
                                             @RequestParam(name = "page", defaultValue = "0") int page,
                                             @RequestParam(name = "size", defaultValue = "5") int size) {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        AppUser appUser = appUserServices.getAppUserByEmail(email);
        return bookServices.getBooksBySellerAndCat(appUser.getId(), catName, page, size);
    }

    @PostMapping("/setBookSold/{bookId}")
    public void setBookSold(@PathVariable Long bookId) {
        bookServices.setBookSold(bookId);
    }

    @DeleteMapping("/{bookId}")
    public void deleteBook(@PathVariable Long bookId) {
        bookServices.deleteBook(bookId);
    }

    @RequestMapping(value = "/addImagetoBook", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> uploadImagetoBook(@RequestParam("file") MultipartFile file, @RequestParam("bookId") Long bookId) throws IOException {
        Book book = bookServices.getBookById(bookId);
        String UPLOAD_DIR = "src/main/resources/static/images/";
        String imagePath = UPLOAD_DIR + book.getId()+"-"+book.getTitle()+".jpg";
        book.setImagePath(imagePath);
        File convertFile = new File(imagePath);
        convertFile.createNewFile();
        FileOutputStream fout = new FileOutputStream(convertFile);
        fout.write(file.getBytes());
        fout.close();
        bookServices.saveBook(book);
        return new ResponseEntity<>("File Uploaded Successfully", HttpStatus.OK);
    }
}
