package org.gi3.bookapp.web;

import org.gi3.bookapp.entities.AppUser;
import org.gi3.bookapp.entities.Book;
import org.gi3.bookapp.services.AppUserServices;
import org.gi3.bookapp.services.BookServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/book")
public class BookRestController {

    private static String UPLOAD_DIR = "src/main/resources/static/images/";

    @Autowired
    private BookServices bookServices;
    @Autowired
    private AppUserServices appUserServices;

    @GetMapping("/{id}")
    public Book getBookById(@PathVariable("id") Long id) {
        return bookServices.getBookById(id);
    }

    @GetMapping("/")
    public Page<Book> getAllBooks(@RequestParam(name = "page", defaultValue = "0") int page,
                                  @RequestParam(name = "size", defaultValue = "5") int size) {
        return bookServices.getAllBooks(page, size);
    }

    @PostMapping("/")
    public Book saveBook(@RequestBody Book book) {
        AppUser appUser = appUserServices.getAppUserById(1L);
        book.setSeller(appUser);
        return bookServices.saveBook(book);
    }

    @GetMapping("/byCat")
    public Page<Book> getBooksByCategory(@RequestParam(name = "catName") String catName,
                                         @RequestParam(name = "page", defaultValue = "0") int page,
                                         @RequestParam(name = "size", defaultValue = "5") int size) {
        return bookServices.getBooksByCategory(catName, page, size);
    }

    @GetMapping("/bySeller")
    public Page<Book> getBooksBySeller(@RequestParam(name = "sellerId") Long sellerId,
                                         @RequestParam(name = "page", defaultValue = "0") int page,
                                         @RequestParam(name = "size", defaultValue = "5") int size) {
        return bookServices.getBooksBySeller(sellerId, page, size);
    }

    @PatchMapping("/setBookSold/{bookId}")
    public void setBookSold(@PathVariable Long bookId) {
        bookServices.setBookSold(bookId);
    }

    @DeleteMapping("/{bookId}")
    public void deleteBook(@PathVariable Long bookId) {
        bookServices.deleteBook(bookId);
    }

    @RequestMapping(value = "/addImagetoBook", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> uploadImagetoBook(@RequestParam("file") MultipartFile file, @RequestParam("bookId") Long bookId) throws IOException {
        Book book = bookServices.getBookById(bookId);
        String imagePath = UPLOAD_DIR + book.getId()+"-"+book.getTitle()+".jpg";
        book.setImagePath(imagePath);
        File convertFile = new File(imagePath);
        convertFile.createNewFile();
        FileOutputStream fout = new FileOutputStream(convertFile);
        fout.write(file.getBytes());
        fout.close();
        bookServices.saveBook(book);
        return new ResponseEntity<>("File Uploaded Successfully", HttpStatus.OK);
    }

    @GetMapping("/downloadImageOfBook")
    public ResponseEntity<Object> getBookImage(@RequestParam Long bookId) throws IOException {
        Book book = bookServices.getBookById(bookId);
        File file = new File(book.getImagePath());
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.parseMediaType(MediaType.IMAGE_JPEG_VALUE)).body(resource);
        return responseEntity;
    }
}
