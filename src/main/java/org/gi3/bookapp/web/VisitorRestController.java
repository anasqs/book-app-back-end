package org.gi3.bookapp.web;

import org.gi3.bookapp.entities.Book;
import org.gi3.bookapp.services.AppUserServices;
import org.gi3.bookapp.services.BookServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/visitor")
public class VisitorRestController {

    @Autowired
    private BookServices bookServices;
    @Autowired
    private AppUserServices appUserServices;

    @GetMapping("/books")
    public Page<Book> getAllBooks(@RequestParam(name = "page", defaultValue = "0") int page,
                                  @RequestParam(name = "size", defaultValue = "5") int size) {
        return bookServices.getAllBooks(page, size);
    }

    @GetMapping("/searchBooks")
    public Page<Book> searchBooks(@RequestParam(name = "key", defaultValue = "") String key,
                                  @RequestParam(name = "page", defaultValue = "0") int page,
                                  @RequestParam(name = "size", defaultValue = "5") int size) {
        return bookServices.searchBooks(key, page, size);
    }

    @GetMapping("/books/byCat")
    public Page<Book> getBooksByCategory(@RequestParam(name = "catName") String catName,
                                         @RequestParam(name = "page", defaultValue = "0") int page,
                                         @RequestParam(name = "size", defaultValue = "5") int size) {
        return bookServices.getBooksByCategory(catName, page, size);
    }

    @GetMapping("/downloadImageOfBook")
    public ResponseEntity<Object> getBookImage(@RequestParam(name = "bookId") Long bookId) throws IOException {
        Book book = bookServices.getBookById(bookId);
        System.out.println(book.getImagePath());
        File file = new File(book.getImagePath());
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.parseMediaType(MediaType.IMAGE_JPEG_VALUE)).body(resource);
        return responseEntity;
    }
}
