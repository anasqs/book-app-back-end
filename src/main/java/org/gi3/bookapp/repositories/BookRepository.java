package org.gi3.bookapp.repositories;

import org.gi3.bookapp.entities.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BookRepository extends JpaRepository<Book, Long> {
    public Page<Book> findBooksByCategory(String catName, Pageable pageable);

    @Query("select book from Book book where book.seller.id = :x")
    public Page<Book> findBooksBySellerId(@Param("x") Long sellerId, Pageable pageable);

    @Query("select book from Book book where book.seller.id = :x and book.category = :y")
    public Page<Book> findBooksBySellerIdAndCate(@Param("x") Long sellerId,@Param("y") String catName, Pageable pageable);

    public Page<Book> findBooksByTitleContains(String title, Pageable pageable);
}
