package org.gi3.bookapp.security.web;

import lombok.AllArgsConstructor;
import org.gi3.bookapp.entities.AppUser;
import org.gi3.bookapp.security.entities.AuthAppUser;
import org.gi3.bookapp.security.models.RegisterModel;
import org.gi3.bookapp.security.services.AccountServices;
import org.gi3.bookapp.services.AppUserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @Autowired
    private AccountServices accountServices;
    @Autowired
    private AppUserServices appUserServices;

    @PostMapping("/register")
    public AppUser register(@RequestBody RegisterModel model) {
        AuthAppUser authAppUser = new AuthAppUser();

        authAppUser.setUsername(model.getEmail());
        authAppUser.setPassword(model.getPassword());

        accountServices.saveUser(authAppUser);
        accountServices.addRoletoUser(authAppUser.getUsername(), "USER");

        AppUser appUser = new AppUser();

        appUser.setName(model.getName());
        appUser.setEmail(model.getEmail());
        appUser.setAddress(model.getAddress());
        appUser.setPhoneNumber(model.getPhoneNumber());

        return appUserServices.saveAppUser(appUser);
    }
}
