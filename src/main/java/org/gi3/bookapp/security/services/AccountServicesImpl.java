package org.gi3.bookapp.security.services;

import org.gi3.bookapp.security.entities.AppRole;
import org.gi3.bookapp.security.entities.AuthAppUser;
import org.gi3.bookapp.security.respositories.AppRoleRepository;
import org.gi3.bookapp.security.respositories.AuthAppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountServicesImpl implements AccountServices{
    @Autowired
    private AuthAppUserRepository userRepository;
    @Autowired
    private AppRoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public AuthAppUser saveUser(AuthAppUser user) {
        String hashPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(hashPassword);
        return userRepository.save(user);
    }

    @Override
    public AppRole saveRole(AppRole role) {
        return roleRepository.save(role);
    }

    @Override
    public void deleteUser(Long userId) { ;
        userRepository.deleteById(userId);

    }

    @Override
    public void deleteRole(Long roleId) {

    }

    @Override
    public void addRoletoUser(String username, String roleName) {
        AuthAppUser user = userRepository.findAuthAppUserByUsername(username);
        AppRole role = roleRepository.findByRoleName(roleName);
        user.getAppRoles().add(role);
    }

    @Override
    public AuthAppUser findUserByUsername(String username) {
        return userRepository.findAuthAppUserByUsername(username);
    }
}
