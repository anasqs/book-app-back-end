package org.gi3.bookapp.security.services;

import org.gi3.bookapp.security.entities.AppRole;
import org.gi3.bookapp.security.entities.AuthAppUser;

import java.util.List;

public interface AccountServices {
    public AuthAppUser saveUser(AuthAppUser user);
    public AppRole saveRole(AppRole role);
    public void deleteUser(Long userId);
    public void deleteRole(Long roleId);
    public void addRoletoUser(String username, String roleName);
    public AuthAppUser findUserByUsername(String username);
}
