package org.gi3.bookapp.security.respositories;

import org.gi3.bookapp.security.entities.AppRole;
import org.gi3.bookapp.security.entities.AuthAppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuthAppUserRepository extends JpaRepository<AuthAppUser,Long> {
    AuthAppUser findAuthAppUserByUsername(String username);
    List<AuthAppUser> findAuthAppUsersByAppRolesContains(AppRole appRole);
}
