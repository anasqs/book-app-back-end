package org.gi3.bookapp.security.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.gi3.bookapp.security.entities.AuthAppUser;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.stream.Collectors;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        AuthAppUser user = null;
        try {
            user = new ObjectMapper().readValue(request.getInputStream(), AuthAppUser.class);
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
        System.out.println(user.getUsername() + " " + user.getPassword());
        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        System.out.println("successful auth");
        User user=(User) authResult.getPrincipal();
        Algorithm algorithm = Algorithm.HMAC256("thisIsTheSecret");
        String jwtAccessToken= JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis()+3600*60*1000))
                .withIssuer(request.getRequestURI())
                .withClaim("roles",user.getAuthorities().stream().map(grantedAuthority -> grantedAuthority.getAuthority()).collect(Collectors.toList()))
                .sign(algorithm);

//        String jwtRefreshToken= JWT.create()
//                .withSubject(user.getUsername())
//                .withExpiresAt(new Date(System.currentTimeMillis()+15*60*1000))
//                .withIssuer(request.getRequestURI())
//                .sign(algorithm);
//        Map<String,String> idToken=new HashMap<>();
//        idToken.put("access-token",jwtAccessToken);
//        idToken.put("refresh-token",jwtRefreshToken);
//        response.setContentType("application/json");
//        new ObjectMapper().writeValue(response.getOutputStream(),idToken);
        response.setHeader("Authorization",jwtAccessToken);
    }
}
