package org.gi3.bookapp.services;

import org.gi3.bookapp.entities.Book;
import org.gi3.bookapp.repositories.BookRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BookServicesImpl implements BookServices {

    private BookRepository bookRepository;

    public BookServicesImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Book getBookById(Long id) {
        return bookRepository.findById(id).get();
    }

    @Override
    public Page<Book> getAllBooks(int page, int size) {
        return bookRepository.findAll(PageRequest.of(page, size));
    }

    @Override
    public Page<Book> getBooksByCategory(String catName, int page, int size) {
        return bookRepository.findBooksByCategory(catName, PageRequest.of(page, size));
    }

    @Override
    public Page<Book> getBooksBySeller(Long sellerId, int page, int size) {
        return bookRepository.findBooksBySellerId(sellerId, PageRequest.of(page, size));
    }

    @Override
    public Book saveBook(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public void setBookSold(Long bookId) {
        Book book = bookRepository.findById(bookId).get();
        boolean soldOut = book.isSoldOut();
        book.setSoldOut(!soldOut);
        bookRepository.save(book);
    }

    @Override
    public void deleteBook(Long bookId) {
        if(!bookRepository.existsById(bookId))
            throw new RuntimeException("This Book does not exist!");
        bookRepository.deleteById(bookId);
    }

    @Override
    public Page<Book> getBooksBySellerAndCat(Long sellerId, String catName, int page, int size) {
        return bookRepository.findBooksBySellerIdAndCate(sellerId, catName, PageRequest.of(page, size));
    }

    @Override
    public Page<Book> searchBooks(String key, int page, int size) {
        return bookRepository.findBooksByTitleContains(key, PageRequest.of(page, size));
    }
}
