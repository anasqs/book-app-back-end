package org.gi3.bookapp.services;

import org.gi3.bookapp.entities.Book;
import org.springframework.data.domain.Page;

public interface BookServices {
    public Book getBookById(Long id);
    public Page<Book> getAllBooks(int page, int size);
    public Page<Book> getBooksByCategory(String catName, int page, int size);
    public Page<Book> getBooksBySeller(Long sellerId, int page, int size);

    public Book saveBook(Book book);

    public void setBookSold(Long bookId);
    public void deleteBook(Long bookId);

    Page<Book> getBooksBySellerAndCat(Long id, String catName, int page, int size);

    Page<Book> searchBooks(String key, int page, int size);
}
