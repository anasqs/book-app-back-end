package org.gi3.bookapp.services;

import org.gi3.bookapp.entities.AppUser;
import org.gi3.bookapp.repositories.AppUserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AppUserServicesImpl implements AppUserServices{

    private AppUserRepository appUserRepository;

    public AppUserServicesImpl(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    @Override
    public AppUser getAppUserById(Long id) {
        return appUserRepository.findById(id).get();
    }

    @Override
    public List<AppUser> getAllAppUsers() {
        return appUserRepository.findAll();
    }

    @Override
    public AppUser saveAppUser(AppUser appUser) {
        return appUserRepository.save(appUser);
    }

    @Override
    public void deleteAppUser(Long id) {
        appUserRepository.deleteById(id);
    }

    @Override
    public AppUser getAppUserByEmail(String email) {
        return appUserRepository.findAppUserByEmail(email);
    }
}
