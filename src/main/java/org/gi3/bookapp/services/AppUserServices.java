package org.gi3.bookapp.services;

import org.gi3.bookapp.entities.AppUser;

import java.util.List;

public interface AppUserServices {
    public AppUser getAppUserById(Long id);
    public List<AppUser> getAllAppUsers();

    public AppUser saveAppUser(AppUser appUser);

    public void deleteAppUser(Long id);

    AppUser getAppUserByEmail(String email);
}
